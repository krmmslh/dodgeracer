package dodgeracer.component;

import java.awt.Graphics;

public abstract class GameObject {

	protected float x, y;
	protected float velX, velY;
	protected REPRESENTATION r;
	
	public GameObject (float x, float y, REPRESENTATION r) {
		this.x = x;
		this.y = y;
		this.r = r;
	}
	
	
	public abstract void tick();
	
	public abstract void render(Graphics g);


	public float getX() {
		return x;
	}


	public void setX(float x) {
		this.x = x;
	}


	public float getY() {
		return y;
	}


	public void setY(float y) {
		this.y = y;
	}


	public float getVelX() {
		return velX;
	}


	public void setVelX(float velX) {
		this.velX = velX;
	}


	public float getVelY() {
		return velY;
	}


	public void setVelY(float velY) {
		this.velY = velY;
	}


	public REPRESENTATION getR() {
		return r;
	}


	public void setR(REPRESENTATION r) {
		this.r = r;
	}
	
	
	
	
}
