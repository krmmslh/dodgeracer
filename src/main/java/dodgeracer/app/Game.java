package dodgeracer.app;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import dodgeracer.component.Player;
import dodgeracer.component.REPRESENTATION;
import dodgeracerg.action.KeyInput;

public class Game extends Canvas implements Runnable{
	

	public static int HEIGHT = 600;

	
	public static int WIDTH = HEIGHT*16/9;

	private Thread thread;
	
	private boolean running;
	private Handler handler;
	
	public Game() {
		handler = new Handler();
		handler.addObject(new Player(Game.WIDTH/12,  Game.HEIGHT*8/12, REPRESENTATION.PLAYER));
		this.addKeyListener(new KeyInput(handler));
		new Window(WIDTH, HEIGHT, "Dodge Racer", this);
	}
	
	
	public synchronized void start () {
		thread = new Thread(this);
		thread.start();
		running=true;
	}

	
	public synchronized void stop () {
		
		try {
			thread.join();
			running = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void run () {
		this.requestFocus();
		long lastTime = System.nanoTime();
		double  amountOfTicks = 60.0;
		double ns = 1_000_000_000 / amountOfTicks; 
		double delta = 0;
		long timer = System.currentTimeMillis();
		int frames = 0;
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				tick();
				delta--;
			}
			if (running) {
				render();
			}
			frames++;
			
			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				frames = 0;
			}
		}
		stop();
	}
	

	private void render() {
		BufferStrategy  bs = this.getBufferStrategy();
		if ( bs == null ) {
			this.createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.black);
		g.fillRect(0, 0 , WIDTH, HEIGHT);
		g.setColor(Color.green);
		g.fillRect(0, (Game.HEIGHT*8/12)+38, Game.WIDTH, 10);
		handler.render(g);
		g.dispose();
		
		bs.show();
		
	}


	private void tick() {
		// TODO Auto-generated method stub
		
	}


	public static void main(String[] args) {
		new Game();
	}
}
